var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Animal = /** @class */ (function () {
    function Animal() {
    }
    Animal.prototype.move = function (distanciaEmMetros) {
        if (distanciaEmMetros === void 0) { distanciaEmMetros = 0; }
        console.log("Animal corre essa " + distanciaEmMetros + "m");
    };
    return Animal;
}());
var Cachorro = /** @class */ (function (_super) {
    __extends(Cachorro, _super);
    function Cachorro() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cachorro.prototype.late = function () {
        console.log('AuAu! AuAu!');
    };
    return Cachorro;
}(Animal));
var cachorro = new Cachorro();
cachorro.late();
cachorro.move(10);
cachorro.late();
var Leopardo = /** @class */ (function (_super) {
    __extends(Leopardo, _super);
    function Leopardo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Leopardo.prototype.Ruge = function () {
        console.log("URRGH! URRGH!");
    };
    Leopardo.prototype.Predador = function () {
        console.log("Pegou a caça");
    };
    return Leopardo;
}(Animal));
var leoleo = new Leopardo();
leoleo.Ruge();
leoleo.move(100);
leoleo.Predador();
