class Animal{
    move(distanciaEmMetros: number = 0){
       console.log(`Animal corre essa ${distanciaEmMetros}m`);
    }
}
    class Cachorro extends Animal{
        late(){
            console.log('AuAu! AuAu!');
        }
    }
    const cachorro = new Cachorro();
    cachorro.late();
    cachorro.move(10);
    cachorro.late();

    class Leopardo extends Animal{
        Ruge(){
        console.log("URRGH! URRGH!");
        }
        Predador(){
        console.log("Pegou a caça");
        }
    }
    const leoleo =new Leopardo();
    leoleo.Ruge();
    leoleo.move(100);
    leoleo.Predador();